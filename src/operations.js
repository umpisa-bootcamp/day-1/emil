const sum = (x, y) => {
    if(x === null && y === null) return null;
    return x + y;
}

const subtract = (x, y) => {
    if(x === null && y === null) return null;
    return x - y;
}

const multiply = (x, y) => {
    if(x === null && y === null) return null;
    return x * y;
}

const divide = (x, y) => {
    if(x === null && y === null) return null;
    return x / y;
}

module.exports = {
    sum,
    subtract,
    multiply,
    divide
}

