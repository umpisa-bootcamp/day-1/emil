const assert = require('assert')
const { sum, subtract, multiply, divide } = require('../src/operations');


// Add
describe('Summation Function', () => {
        // Positive
        it('Should sum 5 and 3 to equal to 8', () =>{
            assert.equal(sum(5,3), 8)
        })
        // Negative
        it('Should not add 5 and 2 to 7', () => {
            assert.equal(sum(5, 3) === 7, false)
        })
        // Edge Case
        it('Should not add null and null', () => {
            assert.equal(sum(null, null), null)
        })

})


// Subtract
describe('Subtraction Function', () => {
    // Positive
    it('5 minus 3 should equal to 2', () =>{
        assert.equal(subtract(5,3), 2)
    })
    // Negative
    it('5 minus 3 should not equal to 4', () => {
        assert.equal(subtract(5, 3) === 4, false)
    })
    // Edge Case
    it('Null subtracted to null should return null', () => {
        assert.equal(subtract(null, null), null)
    })

})

// Multiply

describe('Multiply Function', () => {
    // Positive
    it('5 multiplied to 1 should equal to ', () =>{
        assert.equal(multiply(5,1), 5)
    })
    // Negative
    it('5 multiplied to 1 should not equal to 4', () => {
        assert.equal(multiply(5, 1) === 4, false)
    })
    // Edge Case
    it('Null subtracted to null should return null', () => {
        assert.equal(multiply(null, null), null)
    })

})


describe('Divide Function', () => {
    // Positive
    it('10 divided by to 2 should equal to ', () =>{
        assert.equal(divide(10,2), 5)
    })
    // Negative
    it('10 divided by to 2 should not equal to 4', () => {
        assert.equal(divide(10, 2) === 4, false)
    })
    // Edge Case
    it('Null divided to null should return null', () => {
        assert.equal(divide(null, null), null)
    })

})
